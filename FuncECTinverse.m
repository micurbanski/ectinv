function [ControlMatrix, InputMatrix, OutputMatrix, vect_lin, vect_rot] = FuncECTinverse( vect_lin, vect_rot, MatMatrixSet, opt_cycle )

Time_st=time();         % start time measurements

fprintf('\n\n *** Calculation of set of MatMatrixes... \n');

% ---- Control variables -----

ShowMsgSteps = 0;	% Show messages each step of control

ControlDispInterval= 40; % number of seconds for saving control information

NoCores = 45;            % number of working cores. Core "0" for Octave

MaxTimeCalc = 3500;     % maximal time for ELMER calculations (s)

NumCorrAccept = 0;      % maximal number of accepted corrections

% ---- Preparation of catalogue for each process -----

fprintf('\n Preparation of disk space environment... ');

fprintf('\n Number of working cores: %1.0f  Core 0 reserved for Octave \n',NoCores);

WorkDir = ['Working directory: ' pwd()];
fprintf(" %s \n",WorkDir);
WorkDir = pwd();

for i=1:NoCores,
    DirName=['Work',num2str(i,'%03i')];
    [status, msg, msgid] = mkdir(WorkDir,DirName);
    D1=[WorkDir '/Work/*.*'];
    D2=[WorkDir '/' DirName];
    copyfile(D1,D2,'f');
    D1=[WorkDir '/Work/*'];
    copyfile(D1,D2,'f');
    fprintf('\n Core: %i Directory: %s ',i,DirName);
end;

clear D1 D2 DirName status msg msgid y fi wyn;

fprintf('\n ok.\n');

fprintf("\n-------------------\n\n");

% ---- Preparation of the control matrixes ----

fprintf('\n Control matrixes preparation... ');

InputMatrix=[];

for d=1:size(MatMatrixSet,3),
  for i=1:numel(vect_lin),		% linear positions
    for j=1:numel(vect_rot),       % rotation
        InputMatrix=[InputMatrix; d i j vect_lin(i) vect_rot(j)]; % d,i,j and lin,rot
    end
  end
end

OutputMatrix=zeros(size(InputMatrix,1),2);  % space for Fi_re Fi_im

ControlMatrix=zeros(size(InputMatrix,1),8); 
% (1) state: 0 - nothing, 1 - running, 2 - done, 3 - malfunction
% (2) correction required: 0 - no, 1 - yes
% (3) corrections: 0 - no correction, 1 - 1st correction, 2 - 2nd corection etc.
% (4) calculation start time
% (5) calculation end time
% (6) process number: in
% (7) process number: out
% (8) process number: pid

CoresMatrix=zeros(NoCores,2);

% (1) state: 0 - iddle, 1 - busy
% (2) no of work (from Input Matrix)

fprintf('ok.\n');
fprintf(' Number of lin values: %i, rotation values: %i, cycles: %i \n',numel(vect_lin), numel(vect_rot), size(InputMatrix,1));
fprintf("\n\n-------------------\n\n");


% -----------------------------------------
% --- Main loop of calculation controll ---
% -----------------------------------------

WorkNotFinished=1;  % work finished flag
Time_pt=time();     % time control of information display

while(WorkNotFinished)

% point 1 - find free core and assign new process

FreeCores = find(CoresMatrix(:,1)==0);
ProcesToStart = find(ControlMatrix(:,1)==0);

Nfc=size(FreeCores,1);
Npts=size(ProcesToStart,1);

if ((Nfc > 0) && (Npts > 0))
 
   d=InputMatrix(ProcesToStart(1),1);
   x=InputMatrix(ProcesToStart(1),2);
   fi=InputMatrix(ProcesToStart(1),3);    
   x_val=InputMatrix(ProcesToStart(1),4);
   fi_val=InputMatrix(ProcesToStart(1),5);
   
   if (ControlMatrix(ProcesToStart(1),2)==1)
     fi_val=fi_val+ControlMatrix(ProcesToStart(1),3).*2; % first data
     fprintf('Restarting proces - cycle: %i ',ControlMatrix(ProcesToStart(1),3));
   end
        
   core=FreeCores(1);                  % first free core

   [perme_array, elcond_array]= Mat2Phys(squeeze(MatMatrixSet(:,:,d)));   % --- Physical matrixes ---

   [PSt, in, out, pid]=StartProcess(x,fi,x_val,fi_val,perme_array,elcond_array,core); % Start calculation
   
   if ShowMsgSteps
	    fprintf('Start process %i on core %i \n',ProcesToStart(1),core);
	 end
	
   CoresMatrix(core,1)=1;                  % working
   CoresMatrix(core,2)=ProcesToStart(1);   % no of point
           
   ControlMatrix(ProcesToStart(1),1)=1;    % working
   ControlMatrix(ProcesToStart(1),4)=PSt;  % calculation start time
   ControlMatrix(ProcesToStart(1),6)=in;
   ControlMatrix(ProcesToStart(1),7)=out;
   ControlMatrix(ProcesToStart(1),8)=pid;  % process handles to find it 
   
end  % else no free cores or no processes to do

% point 2 - check the state of processes working
%    - finish with flux density calculation and core is free
%    - finish process with malfunction flag due to: 
%      - time is to long,
%      - Elmer finished without results,
%      process will be repeated or malfunction flag is set.

ProcesesToCheck = find(CoresMatrix(:,1)==1);  % list of running processes

if (size(ProcesesToCheck,1) > 0)
    
    for i=1:size(ProcesesToCheck,1),  % assign process

        ProcesNum=CoresMatrix(ProcesesToCheck(i),2); % get no. of work
        ProcesCore=ProcesesToCheck(i);
        
        DirName=['Work',num2str(ProcesCore,'%03i')];
               
        FEMresF=[WorkDir '/' DirName '/magn_flux.dat'];
        FEMfinF=[WorkDir '/' DirName '/FEM_done.txt'];

        FEMres=exist(FEMresF);
        FEMfin=exist(FEMfinF);
        
        if (FEMfin==2)      % is process finished?
            
            fclose(ControlMatrix(ProcesNum,6));
            fclose(ControlMatrix(ProcesNum,7));
            waitpid(ControlMatrix(ProcesNum,8));    % close process
            [err,msg]=unlink(FEMfinF);              % remove flag
            
            if ShowMsgSteps
        	     fprintf("Proces %i in core %i is finished ",ProcesNum, ProcesCore);
        	  end
            
            if (FEMres==2)  % is process finished succesfully?
            
               if ShowMsgSteps
        	        fprintf("sucessfully. \n");
        	     end
            
               [Fi_re,Fi_im]=ReadValueFi(FEMresF); % read value of Fi
               OutputMatrix(ProcesNum,1)=Fi_re;
               OutputMatrix(ProcesNum,2)=Fi_im;
        
               CoresMatrix(ProcesCore,1)=0;     % core is free
                
               ControlMatrix(ProcesNum,1)=2;    % process is done
               ControlMatrix(ProcesNum,2)=0;    % no correction required
               ControlMatrix(ProcesNum,5)=time();    % time of end                                             
            
            else % is process finished NOT succesfully?
	    
	             if ShowMsgSteps
		              fprintf("with errors. ");
		           end
 
                 % process will be repeated 
               CoresMatrix(ProcesCore,1)=0;     % core is free

               ControlMatrix(ProcesNum,1)=0;    % reset
               ControlMatrix(ProcesNum,2)=1;    % process to be repeated
               ControlMatrix(ProcesNum,3)=ControlMatrix(ProcesNum,3)+1;
                                              % increase correction
                                                                   
            end
          
        else            % process is not finished    
            
            TimeFEM=time()- ControlMatrix(ProcesNum,4);
            
            if (TimeFEM>MaxTimeCalc)    % process is to long?  
                
                fprintf("\n FEM phase to long. Process %i core %i will be repeated. \n", ProcesNum, ProcesCore);
         		
                kill_subs(ControlMatrix(ProcesNum,8));
                fclose(ControlMatrix(ProcesNum,6));
                fclose(ControlMatrix(ProcesNum,7));
                waitpid(ControlMatrix(ProcesNum,8));    % zamknij
                
                CoresMatrix(ProcesCore,1)=0;     % reset
                
                ControlMatrix(ProcesNum,1)=0;    % reset
                ControlMatrix(ProcesNum,2)=1;    % process to be repeated
                ControlMatrix(ProcesNum,3)=ControlMatrix(ProcesNum,3)+1;
                                      % increase correction
				        [err,msg]=unlink(FEMfinF);      % remove flag
                [err,msg]=unlink(FEMresF);      % remove accidental results
   
            end
                         
        end
        
        if (ControlMatrix(ProcesNum,3)>NumCorrAccept)
           ControlMatrix(ProcesNum,1)=(3);    % enough
           if ShowMsgSteps
        	    fprintf("No success. Point terminated. \n");
           end
        % else
           %if ShowMsgSteps
              %fprintf("Point %i will be repeated \n",ProcesNum);
           %end
                
        end
        
    end
    
end

% point 3 - check if work finished and present results

% check if all is done (with results or malfunction)

N1=size((find(ControlMatrix(:,1)==(3))),1);
N2=size((find(ControlMatrix(:,1)==2)),1);
N3=size(ControlMatrix,1);

if N1+N2>=N3
   WorkNotFinished=0;       % main while loop is done
end

% display control results to the screen and file

if (time()-Time_pt)>ControlDispInterval      % each given number seconds
    Time_pt=time();

%    clc;
%    fprintf('\n\n### FTECTmain ###\n\n');
%    fprintf('Process time: %4.2f (min)\n',(time()-Time_st)./60);
%    fprintf('Process time: %4.2f (hours)\n\n',(time()-Time_st)./3600);
%
%    fprintf('Total: %i Done: %i Failed: %i\n\n',N3,N2,N1);
%    for i=1:NoCores,
%
%        %ProcTV=ControlMatrix(CoresMatrix(i,2),6)-ControlMatrix(CoresMatrix(i,2),5);
%        if (CoresMatrix(i,1)==1)
%            ProcTV=time()-ControlMatrix(CoresMatrix(i,2),4);
%            else
%            ProcTV=0;
%        end
%       
%        fprintf('Core: %2i Process: %6i State: %1i Phase: %1i Resol: %1i Time: %4.0f (s)\n',i,CoresMatrix(i,2),CoresMatrix(i,1),CoresMatrix(i,3),ControlMatrix(CoresMatrix(i,2),4),ProcTV);
%    end
%    
%    fprintf('\n\nInput matrix size %i %i \n',size(InputMatrix,1), size(InputMatrix,2));
    
    FID_TV = fopen('AAA_state.txt', 'w');
    fprintf(FID_TV,'FTECTmain\n\n');
    fprintf(FID_TV,'Cycle of optimisation: %i \n',opt_cycle);
    fprintf(FID_TV,'Process time: %4.2f (min)\n',(time()-Time_st)./60);
    fprintf(FID_TV,'Process time: %4.2f (hours)\n\n',(time()-Time_st)./3600);

    fprintf(FID_TV,'Total: %i Done: %i Failed: %i\n\n',N3,N2,N1);
    for i=1:NoCores,

        if (CoresMatrix(i,1)==1)
            ProcTV=time()-ControlMatrix(CoresMatrix(i,2),4);
            else
            ProcTV=0;
        end
       
        if (CoresMatrix(i,2)!=0)
           fprintf(FID_TV,'Core: %2i Process: %6i State: %1i Corection: %1i Time: %4.0f (s)\n',i,CoresMatrix(i,2),CoresMatrix(i,1),ControlMatrix(CoresMatrix(i,2),3),ProcTV);
           end
    end
    fprintf(FID_TV,'\n\nInput Matrix size %i %i \n',size(InputMatrix,1), size(InputMatrix,2));
    fclose(FID_TV);
    
end

pause(0.5); % wait 0.5 sec. to not overload 0 core

end	% end of main while


save _ControlMatrix.txt ControlMatrix
save _InputMatrix.txt InputMatrix
save _OutputMatrix.txt OutputMatrix
save _Steps.txt vect_lin vect_rot

save -binary _ControlMatrix.mat ControlMatrix
save -binary _InputMatrix.mat InputMatrix
save -binary _OutputMatrix.mat OutputMatrix
save -binary _Steps.mat vect_lin vect_rot

fprintf("\n\n Cleaning workspace...");

[out_t,txt]=system("rm -rf Work0*");

fprintf('ok.\n\n All work done in function. \n\n');


end

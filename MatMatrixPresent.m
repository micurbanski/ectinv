function [ wynik ] = MatMatrixPresent(mat_array, mat_char, mat_string)

mat_char=mat_char(1);

fprintf("\n\n %s\n",mat_string);

fprintf("+");
for i=1:size(mat_array,2)
  fprintf("-");
  end
fprintf("+\n");


 for i=1:size(mat_array,1)
    fprintf("|");
    for j=1:size(mat_array,2)
      if (mat_array(i,j)==1)
         fprintf("%s",mat_char);
         else
         if (mat_array(i,j)==0)
             printf(" ");
             else
             fprintf("?");
         end
      end
    end
    fprintf("|\n");      
 end     
      
fprintf("+");
for i=1:size(mat_array,2)
  fprintf("-");
  end

fprintf("+\n\n");

wynik=1;

end
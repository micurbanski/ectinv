function [matrix_out] =  MatrixNorm(matrix_in)

matrix_out=matrix_in-min(min(matrix_in));

matrix_out=matrix_out./max(max(matrix_out));

end 
function [ result_points,result_matrix ] = MatMatrixFNeghibor(mat_array)

result_points=[];
result_matrix=zeros(size(mat_array));


[i_n,j_n]=size(mat_array);

for i=1:i_n
  for j=1:j_n
    
    TestFlag=0;
    
    if mat_array(i,j)==1
      
      if i>1
        if mat_array(i-1,j)==0
          TestFlag=1;
        end
      end
      
      if i<i_n
        if mat_array(i+1,j)==0
          TestFlag=1;
        end
      end
      
      if j>1
        if mat_array(i,j-1)==0
          TestFlag=1;
        end
      end
      
      if j<j_n
        if mat_array(i,j+1)==0
          TestFlag=1;
        end
      end    
      
      if ((i==1) || (i==i_n) || (j==1) || (j==j_n))
         TestFlag=1;
      end
      
      
    end   
      
    if mat_array(i,j)==0
      
      if i>1
        if mat_array(i-1,j)==1
          TestFlag=1;
        end
      end
      
      if i<i_n
        if mat_array(i+1,j)==1
          TestFlag=1;
        end
      end
      
      if j>1
        if mat_array(i,j-1)==1
          TestFlag=1;
        end
      end
      
      if j<j_n
        if mat_array(i,j+1)==1
          TestFlag=1;
        end
      end    
      
    end         
      
        
    
  if TestFlag
  result_points=[result_points; i j];
  result_matrix(i,j)=1;
  end
    
end


end  

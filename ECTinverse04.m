% Software for inverse transformation in EC Tomography system

clear all;
clc;                    % clear variables and screen

Time_st=time();         % start time measurements


% ---- Control variables -----

DrawMeasResults=0;      % draw the measurement results for verification and EXIT

DoDiary = 1;             % diary on / off

ShowMsgSteps = 1;	% Show messages each step of control

ControlDispInterval= 20; % number of seconds for saving control information

NoCores = 45;            % number of working cores. Core "0" for Octave

MaxTimeCalc = 3500;     % maximal time for ELMER calculations (s)

NumCorrAccept = 0;      % maximal number of accepted corrections

ForceMeshesGen =  0;    % force generation of meshes at the begining


setenv("OMP_NUM_THREADS","1");  % one Octave thread
page_screen_output(0);
page_output_immediately(1);     % print immediately


% ---- Start the program ----

fprintf('\n*******************************************************');
fprintf('\n*** Inverse transformation for EC Tomography System ***');
fprintf('\n*******************************************************\n\n');

if DoDiary
   diary on;
end


% ----- Initial Analyse ----

cd("InitialAnalyse");

[x0,y0,quality0,vect_x,vect_y,quality1,results_a,results_p]=Input_data_analyse("Walek_Fe8_2013-02-27_22-50-36.txt");

%results_ref_a=results_a(:,vect_x);
%results_ref_a=results_ref_a(vect_y,:);
%
%results_ref_p=results_p(:,vect_x);
%results_ref_p=results_ref_p(vect_y,:);

results_ref_a=results_a;

results_ref_p=results_p;


if DrawMeasResults

  subplot(1,3,1);
  results_a_sel=results_a(1:x0,:);
  pcolor(results_a_sel);
  title("original");

  [x_red,y_red]=meshgrid(vect_x,vect_y);

  subplot(1,3,2);
  pcolor(x_red,y_red,results_ref_a);
  title("reduced");

  [x,y]=meshgrid(1:101,1:x0);

  results_ref_a_interp=interp2(x_red,y_red,results_ref_a,x,y,"spline");

  subplot(1,3,3);
  pcolor(results_ref_a_interp);
  title("interpolated");
  
  drawnow;
  
  quality_med=sum(sum(abs(results_a_sel-results_ref_a_interp)))./numel(results_a_sel).*100;
  quality_max=max(max(abs(results_a_sel-results_ref_a_interp))).*100;
  fprintf("\n mean deviation of interpolated results from measurements results: %2.1f%% \n",quality_med);
  fprintf(" maximal deviation of interpolated results from measurements results: %2.1f%% \n",quality_max);
  
  return; % don't perform optimisation
  
  end


cd ("..");

 
% ---- Generation of meshes considering x movement ----

fprintf('\n Generation of meshes considering x movement \n');
 
vect_lin=vect_y-x0;
vect_rot=360-(vect_x-1).*(20./2018.*360);     % Calculate physical values

% --- Shorten non-physical calculations ---
% vect_lin=[-10 0];
% vect_rot=[0 120];
% results_ref_p=results_ref_p(1:2,1:2);
% results_ref_a=results_ref_a(1:2,1:2);


MeshesDir='MeshesDir';
mkdir(MeshesDir);

for i=1:numel(vect_lin)
    
    fprintf(' Generate mesh no. %02i of %02i...',i,numel(vect_lin));
    
    filename=[MeshesDir,char(47),"Mesh",num2str(i,'%02i'),char(47),"/ECTmesh/mesh.boundary"];

    if ((exist(filename)==2) && (ForceMeshesGen==0))
       fprintf("Previous mesh used... ");
    else

    filename=[MeshesDir,char(47),"Mesh",num2str(i,'%02i')];
    mkdir(filename);
    
    filename=[MeshesDir,char(47),"Mesh",num2str(i,'%02i'),char(47),"ECTmesh.geo"];
    wyn=GEOWriteX(filename,vect_lin(i));
    
    filename=[MeshesDir,char(47),"Mesh",num2str(i,'%02i'),char(47),"mesh_gen"];
    wyn=BATCHMeshWrite(filename);
   
    cd(MeshesDir);
    
    filename=["Mesh",num2str(i,'%02i')];
    
    cd(filename);
    
    [in, out, pid] = popen2 ("./mesh_gen");

    while(exist("MeshReady.txt"))~=2
  	pause(0.5);
  	end;

    fclose(in);
    fclose(out);
    waitpid(pid);    % wykonaj skrypt i zamknij
    unlink("MeshReady.txt");

    cd ("..");
    cd ("..");
    
    end
    
    fprintf('ok. \n');
    
    end

fprintf("\n-------------------\n\n");


% --- Start calculation function ---

MatMatrix = MatMatrixGen_Cut(1,3,3);

opt_cycle=1;

while (opt_cycle<10)

fprintf("\n\n Optimisation cycle %i \n\n",opt_cycle);


MatMatrixPresent(MatMatrix,'#',"Material Matrix");
MatMatrixPresentF("MatMatrix",opt_cycle,MatMatrix,'#',"Material Matrix");

[PointsList,PointsMatrix] = MatMatrixFNeghibor(MatMatrix);

MatMatrixPresent(PointsMatrix,'N',"Neghibor Matrix");
MatMatrixPresentF("NMatrix",opt_cycle,PointsMatrix,'N',"Neghibor Matrix");


MatMatrixSet = MatMatrixGMatOpt(MatMatrix,PointsMatrix);

[ControlMatrix, InputMatrix, OutputMatrix, vect_lin, vect_rot] = FuncECTinverse( vect_lin, vect_rot, MatMatrixSet, opt_cycle);

ResultMatrix_a=zeros(max(InputMatrix)(2),max(InputMatrix)(3),max(InputMatrix)(1));
%ResultMatrix_p=zeros(max(InputMatrix)(2),max(InputMatrix)(3),max(InputMatrix)(1));

for i=1:size(OutputMatrix,1),
    ampl=((OutputMatrix(i,1)).^2+(OutputMatrix(i,2)).^2).^0.5;
    ResultMatrix_a(InputMatrix(i,2),InputMatrix(i,3),InputMatrix(i,1))=ampl;
    %ResultMatrix_p(InputMatrix(i,2),InputMatrix(i,3),InputMatrix(i,1))=asin(OutputMatrix(i,2)./ampl);
end

for i=1:size(ResultMatrix_a,3)
    ResultMatrix_a(:,:,i)=MatrixNorm(squeeze(ResultMatrix_a(:,:,i)));
    %ResultMatrix_p(:,:,i)=MatrixNorm(squeeze(ResultMatrix_p(:,:,i)));
end

Quality_a=[];
%Quality_p=[];

for i=1:size(ResultMatrix_a,3)
    Quality_a=[Quality_a; sum(sum((results_ref_a-squeeze(ResultMatrix_a(:,:,i))).^2))];
    %Quality_p=[Quality_p; sum(sum((results_ref_p-squeeze(ResultMatrix_p(:,:,i))).^2))];
end

ChangeMatrix=zeros(16,16);

for i=2:size(Quality_a,1)

    if (Quality_a(i,1)<Quality_a(1,1))
        ChangeMatrix(PointsList(i-1,1),PointsList(i-1,2))=1;
    end
 end
 
MatMatrixPresent(ChangeMatrix,"C","Change Matrix");
MatMatrixPresentF("ChangeMatrix",opt_cycle,ChangeMatrix,"C","Change Matrix");

save -v6 all

MatMatrix=xor(MatMatrix, ChangeMatrix);

MatMatrixPresentF("NewMatrix",opt_cycle,MatMatrix,"#","New Matrix");

opt_cycle=opt_cycle+1;

end



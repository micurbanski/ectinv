function [ PSt, in, out, pid ] = StartProcess( x, fi, x_val, fi_val, perme_array, elcond_array, core )
%STRARTPROCESS Start the process - FEM phase

% Clear space of core

D1=['Work',num2str(core,'%03i')];

cd(D1);

[output, text]=system("./clearspace"); 

cd ('..');

% Copy meshes

MeshName=['MeshesDir/Mesh',num2str(x,'%02i')];
D1=[MeshName '/*.*'];

DirName=['Work',num2str(core,'%03i')];
D2=[DirName];

copyfile(D1,D2,'f');

D1=[MeshName '/*'];
copyfile(D1,D2,'f');

% Crate .f90 file

D1=['Work',num2str(core,'%03i'),'/perme_cond_p2.f90'];

F90WriteX(D1, perme_array, elcond_array, x_val, cos(fi_val./360.*2.*pi), sin(fi_val./360.*2.*pi));

% create batch file

D1=['Work',num2str(core,'%03i')];

wynik = BATCHFEMWrite(D1, core);

% start FEM batch covering f90 build

D1=['Work',num2str(core,'%03i')];

cd(D1);

[in, out, pid]=popen2("./FEMphase_go");

cd ('..');

PSt=time();

end


# README #

### General info ###

Software environment for **Eddy Current Tomography with Inverse Transformation** for non-destructive testing of conductive elements.

### Setup ###

* [ElmerFEM](https://github.com/ElmerCSC/elmerfem)
* [Netgen (5.3.1)](http://sourceforge.net/projects/netgen-mesher/)
* [Octave (3.4.3 - 3.8.2)](http://www.gnu.org/software/octave/)


### Further info ###

* ["Eddy current." Wikipedia, The Free Encyclopedia. Wikipedia, The Free Encyclopedia, 1 Apr. 2015. Web. 19 May. 2015.](http://en.wikipedia.org/wiki/Eddy_current)
* [Jacek Salach et al 2014 Meas. Sci. Technol. 25, **Eddy current tomography for testing of ferromagnetic and non-magnetic materials**](http://iopscience.iop.org/0957-0233/25/2/025902/article)
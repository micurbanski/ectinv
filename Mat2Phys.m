function [ perme_array, elcond_array ] = Mat2Phys(MatMatrix)

el_cond=1.45;	% * 1e6
magn_perm=1e3;

elcond_array=MatMatrix.*el_cond;

perme_array=MatMatrix.*(magn_perm-1);
perme_array=perme_array+1;

end


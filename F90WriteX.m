function [ wynik ] = F90WriteX( nazwa, perme_array, elcond_array, x_val, cf, sf )
    %Write Permeability and ELectric Conductivity Array generated in matlab,
    %linear displacement 'x_step',sinus and cosinus of array rotation
    % into fortran *.f90 file (!perme_cond_p2.f90!)

%Array transposition for Fortran column-major initialization
perme_arrayT = perme_array';
elcond_arrayT = elcond_array';

%Permeability array header:
a{1}='    DATA perme_array &';
a{2}='    / &';

%Permeability Array Data Initialization
a{3}=[num2str(perme_arrayT(1,:),'%1.0fd0, '),' &'];
a{4}=[num2str(perme_arrayT(2,:),'%1.0fd0, '),' &'];
a{5}=[num2str(perme_arrayT(3,:),'%1.0fd0, '),' &'];
a{6}=[num2str(perme_arrayT(4,:),'%1.0fd0, '),' &'];
a{7}=[num2str(perme_arrayT(5,:),'%1.0fd0, '),' &'];
a{8}=[num2str(perme_arrayT(6,:),'%1.0fd0, '),' &'];
a{9}=[num2str(perme_arrayT(7,:),'%1.0fd0, '),' &'];
a{10}=[num2str(perme_arrayT(8,:),'%1.0fd0, '),' &'];
a{11}=[num2str(perme_arrayT(9,:),'%1.0fd0, '),' &'];
a{12}=[num2str(perme_arrayT(10,:),'%1.0fd0, '),' &'];
a{13}=[num2str(perme_arrayT(11,:),'%1.0fd0, '),' &'];
a{14}=[num2str(perme_arrayT(12,:),'%1.0fd0, '),' &'];
a{15}=[num2str(perme_arrayT(13,:),'%1.0fd0, '),' &'];
a{16}=[num2str(perme_arrayT(14,:),'%1.0fd0, '),' &'];
a{17}=[num2str(perme_arrayT(15,:),'%1.0fd0, '),' &'];
a{18}=[num2str(perme_arrayT(16,1:15),'%1.0fd0, '),' ',num2str(perme_arrayT(16,16),'%1.0fd0 '),' &'];
a{19}='    /';
%
%%Electric Conductivity Array Header
a{20}='     DATA elcond_array &';
a{21}='    / &';
%
%%Electric Conductivity Data Initialization
a{22}=[num2str(elcond_arrayT(1,:),'%1.2fd6, '),' &'];
a{23}=[num2str(elcond_arrayT(2,:),'%1.2fd6, '),' &'];
a{24}=[num2str(elcond_arrayT(3,:),'%1.2fd6, '),' &'];
a{25}=[num2str(elcond_arrayT(4,:),'%1.2fd6, '),' &'];
a{26}=[num2str(elcond_arrayT(5,:),'%1.2fd6, '),' &'];
a{27}=[num2str(elcond_arrayT(6,:),'%1.2fd6, '),' &'];
a{28}=[num2str(elcond_arrayT(7,:),'%1.2fd6, '),' &'];
a{29}=[num2str(elcond_arrayT(8,:),'%1.2fd6, '),' &'];
a{30}=[num2str(elcond_arrayT(9,:),'%1.2fd6, '),' &'];
a{31}=[num2str(elcond_arrayT(10,:),'%1.2fd6, '),' &'];
a{32}=[num2str(elcond_arrayT(11,:),'%1.2fd6, '),' &'];
a{33}=[num2str(elcond_arrayT(12,:),'%1.2fd6, '),' &'];
a{34}=[num2str(elcond_arrayT(13,:),'%1.2fd6, '),' &'];
a{35}=[num2str(elcond_arrayT(14,:),'%1.2fd6, '),' &'];
a{36}=[num2str(elcond_arrayT(15,:),'%1.2fd6, '),' &'];
a{37}=[num2str(elcond_arrayT(16,1:15),'%1.2fd6, '),' ',num2str(elcond_arrayT(16,16),'%1.2fd6 '),' &'];
a{38}='    /';
%
%%End of Arrays Initialization
a{39}='';
a{40}='!End of data initialization';
a{41}='';
%
%%Declaration of x_step, sinus and cosinus
a{42}='    REAL(KIND=dp) :: &';

x_step = x_val./1000; %recount to meters
y_db = x_step + (-0.016);
y_ub = x_step + (0.016);

a{43}=['    x_step = ',num2str(x_step,'%1.4f'), 'd0 ,& !x_step - odpowiednik x1'];
a{44}=['    cf = ',num2str(cf,'%1.4f'), 'd0 ,&'];
a{45}=['    sf = ',num2str(sf,'%1.4f'), 'd0 ,&'];

a{46}=['    y_db = ',num2str(y_db,'%1.4f'), 'd0 ,&'];
a{47}=['    y_ub = ',num2str(y_ub,'%1.4f'), 'd0 ,&'];
a{48}='';

%Write to file
FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

wynik=1;

end


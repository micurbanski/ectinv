function [ perme_array, elcond_array ] = TestMatrixes(test)

if test==1e17
    return;
    end
    

x_step=0;
cf=cos(3*pi./2);
sf=sin(3*pi./2);

perme_array=zeros(16,16);
elcond_array=zeros(16,16);

for i=1:16
  for j=1:16

    if ((i-8.5).^2+(j-8.5).^2).^0.5<7
     perme_array(i,j)=1000;
     elcond_array(i,j)=1.45;
    else
        perme_array(i,j) = 1;
        elcond_array(i,j) = 0;
    endif
    
    if ((i<11)&&(i>6)&&(j>10))
     perme_array(i,j)=1;
     elcond_array(i,j)=0;
    endif
    
  end
    
end
    
PresentMatrixes(perme_array, elcond_array);

end


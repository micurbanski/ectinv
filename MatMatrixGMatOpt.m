function [ result_matrix ] = MatMatrixGMatOpt(mat_array,neib_array)

result_matrix=zeros(size(mat_array,1),size(mat_array,2),1);

result_matrix(:,:,1)=mat_array;

d=1;

[i_n,j_n]=size(neib_array);


for i=1:i_n
  for j=1:j_n
   
    if neib_array(i,j)==1
    
    help_array=mat_array;
    
    help_array(i,j)=!help_array(i,j);
    
    d=d+1;
    
    result_matrix(:,:,d)=help_array;
    
    end         
  end
    
end



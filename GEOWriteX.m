function [ wynik ] = GEOWriteX( nazwa, x )
%GEOWRITE Summary of this function goes here
%   Detailed explanation goes here

%a{1}=['$ y0=', num2str(y,'%1.4f')];
%a{2}=['$ cf=', num2str(cos(fi./360.*2.*pi),'%1.4f')];
%a{3}=['$ sf=', num2str(sin(fi./360.*2.*pi),'%1.4f')];

x1=x./1000;  % recalculate to meters

xmin=x1+(-0.016);
xmax=x1+0.016;

a{1}='#';
a{2}='## EC Tomograph Mesh';
a{3}='#';
a{4}='algebraic3d';
a{5}='';
a{6}='solid CoilOut = cylinder ( 0, 0, 0; 0, 0, -0.032; 0.0179 )';
a{7}='	and plane (0, 0, -0.03176; 0, 0, -1)';
a{8}='	and plane (0, 0, -0.017; 0, 0, 1);';
a{9}='';
a{10}='solid CoilIn= cylinder ( 0, 0, 0; 0, 0, -0.032; 0.0074 )';
a{11}='	and plane (0, 0, -0.032; 0, 0, -1)';
a{12}='	and plane (0, 0, -0.016; 0, 0, 1);';
a{13}='';
a{14}='solid Coil = CoilOut and not CoilIn -maxh=0.003;  # 0.003';
a{15}='';
a{16}=['solid Block = plane (', num2str(xmin,'%1.4f'),', -0.05, -0.016; 0, 0, -1) '];
a{17}=['          and plane (', num2str(xmin,'%1.4f'),', -0.05, -0.016; 0, -1, 0) '];
a{18}=['          and plane (', num2str(xmin,'%1.4f'),', -0.05, -0.016; -1, 0, 0) '];
a{19}=['          and plane (', num2str(xmax,'%1.4f'),', 0.05, 0.016; 0, 0, 1)'];
a{20}=['          and plane (', num2str(xmax,'%1.4f'),', 0.05, 0.016; 0, 1, 0) '];
a{21}=['          and plane (', num2str(xmax,'%1.4f'),', 0.05, 0.016; 1, 0, 0) -maxh=0.002;  # 0.0025'];
a{22}='';
a{23}='solid CoilDet = cylinder ( 0, 0, 0.016; 0, 0, 0.025; 0.0148 )';
a{24}='	and plane (0, 0, 0.017; 0, 0, -1)';
a{25}='	and plane (0, 0, 0.02486; 0, 0, 1) -maxh=0.002; # 0.002';
a{26}='';
a{27}='solid Range = sphere (0, 0, 0; 1) ';
a{28}='	 and not Coil';
a{29}='	 and not Block';
a{30}='	 and not CoilDet -maxh=0.3;  # 0.2';
a{31}='';
a{32}='tlo Coil;';
a{33}='';
a{34}='tlo Block;';
a{35}='';
a{36}='tlo CoilDet;';
a{37}='';
a{38}='tlo Range;';


FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

wynik=1;

end


function [ wynik ] = BATCHMeshWrite( filename )
%BATCHWRITE prepare batch files in catalog for core n

a{1}='#!/bin/sh -f';
a{2}='netgen -geofile=ECTmesh.geo -meshfile=ECTmesh.msh -meshfiletype="Gmsh2 Format" -batchmode';
a{3}='ElmerGrid 14 2 ECTmesh.msh -autoclean';
a{4}='rm ECTmesh.msh';
a{5}='touch MeshReady.txt';

FID = fopen(filename, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

[output, text]=system(["chmod 755 ", filename]);    

wynik=1;

end


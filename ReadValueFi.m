function [ Fi_re, Fi_im ] = ReadValueFi( filename )
%ReadValueFi Read Fi re and im

tabl=load(filename);		% ascii load on Octave

Fi_re=(tabl(1)+tabl(3))./2;
Fi_im=(tabl(2)+tabl(4))./2;;

end


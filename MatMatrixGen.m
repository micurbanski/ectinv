function [ MatMatrix ] = MatMatrixGen(test)

if test==1e17
    return;
    end

MatMatrix=zeros(16,16);

for i=1:16
  for j=1:16

    if ((i-8.5).^2+(j-8.5).^2).^0.5<8
     MatMatrix(i,j)=1;
    else
        MatMatrix (i,j) = 0;
    end
    
%    if ((i<11)&&(i>6)&&(j>10))
%     MatMatrix(i,j)=0;
%    end
%    
  end
    
end

end


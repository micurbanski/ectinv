function [ wynik ] = MatMatrixPresentF(filename,numer,mat_array, mat_char, mat_string)

mat_char=mat_char(1);

file1=[filename, num2str(numer,'%i')];

FID=fopen(filename,'w');

fprintf(FID,"\n\n %s\n",mat_string);

fprintf(FID,"+");
for i=1:size(mat_array,2)
  fprintf(FID,"-");
  end
fprintf(FID,"+\n");


 for i=1:size(mat_array,1)
    fprintf(FID,"|");
    for j=1:size(mat_array,2)
      if (mat_array(i,j)==1)
         fprintf(FID,"%s",mat_char);
         else
         if (mat_array(i,j)==0)
             fprintf(FID," ");
             else
             fprintf(FID,"?");
         end
      end
    end
    fprintf(FID,"|\n");      
 end     
      
fprintf(FID,"+");
for i=1:size(mat_array,2)
  fprintf(FID,"-");
  end

fprintf(FID,"+\n\n");

fclose(FID);

wynik=1;

end
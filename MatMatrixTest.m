
MatMatrix = MatMatrixGen(1);

MatMatrixPresent(MatMatrix,'#',"Material Matrix");

[perme_array, elcond_array]=Mat2Phys(MatMatrix);

[PointsList,PointsMatrix]=MatMatrixFNeghibor(MatMatrix);

MatMatrixPresent(PointsMatrix,'N',"Neghibor Matrix");

MatrixForOpt= MatMatrixGMatOpt(MatMatrix,PointsMatrix);

MatMatrixPresent(squeeze(MatrixForOpt(:,:,19)),'H',"Opt. Matrix no. 22");


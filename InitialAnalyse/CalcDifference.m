function [difference] = CalcDifference (xx,yy,wyn_a,wyn_f,xxi,yyi)

xx_sel=xx(:,xxi);
xx_sel=xx_sel(yyi,:);

yy_sel=yy(:,xxi);
yy_sel=yy_sel(yyi,:);

wyn_a_sel=wyn_a(:,xxi);
wyn_a_sel=wyn_a_sel(yyi,:);

wyn_f_sel=wyn_f(:,xxi);
wyn_f_sel=wyn_f_sel(yyi,:);

wyn_a_calc=interp2(xx_sel,yy_sel,wyn_a_sel,xx,yy,'spline');
wyn_f_calc=interp2(xx_sel,yy_sel,wyn_f_sel,xx,yy,'spline');

difference=sum(sum((wyn_a-wyn_a_calc).^2))+sum(sum((wyn_f-wyn_f_calc).^2));

end

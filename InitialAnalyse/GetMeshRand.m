function wyn = GetMeshRand(a,maximum)
% a - number of points from 2:maximum-1 range
%55 maximum - tha range of meshing
% wyn = [1 a-points maximum]

wyn = [1 sort(randperm1	 (maximum-2,a)+1) maximum];

end

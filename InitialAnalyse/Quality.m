function [qual]=Quality (wyn_a,wyn_a_reflected,wyn_f,wyn_f_reflected)

%***This function calculates the mean square error between reflected and initial data. MSE is normalized to the input array size.
%
%
%***Input arguments:	'wyn_a'				-initial amplitude data.
%						'wyn_a_reflected'	-reflected amplitude data.
%						'wyn_f'				-initial phase shift data.
%						'wyn_f_reflected'	-reflected phase shift data.
%
%***Output arguments:	'qual'				-normalized mean square error between initial and reflected input data.

[a b]=size(wyn_a);
qual=(sum(sum((wyn_a-wyn_a_reflected).^2))+sum(sum((wyn_f-wyn_f_reflected).^2)));%./(a.*b);

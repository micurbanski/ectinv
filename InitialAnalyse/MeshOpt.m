function [xx_opt, yy_opt, quality] = MeshOpt (xx,yy,wyn_a,wyn_f,xxi,yyi)

xxo=xxi;
yyo=yyi;
xnow=xxo;
ynow=yyo;
flag_opt=1;

quality=CalcDifference(xx,yy,wyn_a,wyn_f,xxo,yyo);

fprintf('\n quality_init= %f',quality);

while flag_opt==1

  flag_opt=0;   										% no optimisation steps

  for i=2:size(xxi,2)-1      							% optimisation of xxi
    
   
    xxo_=xxo;											% up
    
    if xxo_(i)<xxo_(i+1)-1 
      xxo_(i)=xxo_(i)+1;
  
      quality_=CalcDifference(xx,yy,wyn_a,wyn_f,xxo_,yyo);   

      if quality_<quality 
        xxo=xxo_;
        quality =quality_;
        flag_opt=1; 
       end  
     end 
    
   
    
    xxo_=xnow;											 % down
    
    if xxo_(i)>xxo_(i-1)+1 
      xxo_(i)=xxo_(i)-1;
  
      quality_=CalcDifference(xx,yy,wyn_a,wyn_f,xxo_,yyo);   

      if quality_<quality 
        xxo=xxo_;
        quality =quality_;
        flag_opt=1; 
      end  
     end 
    
  end  													%of for
  
  
  for i=2:size(yyi,2)-1      % optimisation of yyi
    
   
    yyo_=yyo;											%up
    
    if yyo_(i)<yyo_(i+1)-1 
      yyo_(i)=yyo_(i)+1;
  
      quality_=CalcDifference(xx,yy,wyn_a,wyn_f,xxo,yyo_);   

      if quality_<quality 
        yyo=yyo_;
        quality =quality_;
        flag_opt=1; 
        end  
     end 
    
   
    
    yyo_=ynow;											% down
    
    if yyo_(i)>yyo_(i-1)+1 
      yyo_(i)=yyo_(i)-1;
  
      quality_=CalcDifference(xx,yy,wyn_a,wyn_f,xxo,yyo_);   

      if quality_<quality 
        yyo=yyo_;
        quality =quality_;
        flag_opt=1; 
        end  
     end 
    
  end  % of for

 xnow=xxo;
 ynow=yyo; 
  
fprintf('\n quality= %f',quality)

end   % of while

xx_opt=xxo;
yy_opt=yyo;
fprintf('\n');

end

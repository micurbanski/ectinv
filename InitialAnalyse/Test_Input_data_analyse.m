clear all
[x0,y0,quality0,vect_x,vect_y,quality1,results_a,results_p,wyn_a,wyn_f]=Input_data_analyse("Walek_Fe8_2013-02-27_22-50-36.txt");


subplot(1,3,1);
results_a_sel=wyn_a(1:x0,:);
pcolor(results_a_sel);
title("original");

%results_ref_a=results_a(:,vect_x);
%results_ref_a=results_ref_a(vect_y,:);
[x_red,y_red]=meshgrid(vect_x,vect_y);

subplot(1,3,2);
pcolor(x_red,y_red,results_a);
title("reduced");

[x,y]=meshgrid(1:101,1:x0);

results_ref_a_interp=interp2(x_red,y_red,results_a,x,y,"spline");

subplot(1,3,3);
pcolor(wyn_a);
title("interpolated");


quality_med=sum(sum(abs(results_a_sel-results_ref_a_interp)))./numel(results_a_sel).*100;
quality_max=max(max(abs(results_a_sel-results_ref_a_interp))).*100;
fprintf("\n mean deviation of interpolated results from measurements results: %2.7f%% \n",quality_med);
fprintf(" maximal deviation of interpolated results from measurements results: %2.7f%% \n",quality_max);



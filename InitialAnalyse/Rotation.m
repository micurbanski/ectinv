function [planeout] = Rotation (plane, point_x, point_y)

%***This function rotates input plane with respect to point described by two coordinates. Rotation is conducted as a mirror reflection of all data in rotation domain with respec to the point.
%Whole process may run in two ways, depending on reflection point coordinate in phase domain (point_y):
%	1.If this coordinate is bigger than midpoint of the 'plane', whole rotation data is reflected.
%	2.If this coordinate is lower  than midpoint of the 'plane', rotation data only to point_y are reflected.
%
%
%***Input arguments:	'plane' 	-array of data.
%						'point_x'	-X coordinate of rotation point.
%						'point_Y'	-Y coordinate of rotation point.
%
%***Output arguments:	'planeout'	-output array of rotate.

[a b]=size(plane);		

point_x=min(a,point_x);
point_y=min(b,point_y);												%Safeguard against too big values of rotation poin coordinates.

if (point_y >= ceil(b./2))											%Point_y coordinate is bigger than midpoint of the 'plane'. Whole rotation data is reflected.
	
	for i=1:point_x													%For loop in linear movement domain.
		for j=1:b													%For loop in rotation domain.	

			d_x=point_x-i+1;										%Calculation of indexes of points symmetrical to currently analyzed point (i,j) with respect to the declared midpoint.
			d_y=point_y-j+1;
			x_odb=point_x+d_x;
			y_odb=point_y+d_y;
		
			planeout(i,j)= plane(i,j);								%Coping of values from input plane to output plane.
			planetemp(x_odb,y_odb)=plane(i,j);						%Main reflection function. Coping the data from old coordinates to symmetrical coordinates in temporary plane.
			
		end															%End of for loop in rotation domain.																										
	end																%End of for loop in linear movement domain.

	planetemp(1:point_x,:)=[];										%Removal of unnecessary empty lines in temporary plane.
	
	licz=0;															%Begginig of removal of unnecessary empty columns in temporary plane.
	s=0;
	for i=1:(size(planetemp)(2))
		s=sum(planetemp(:,i));
		if s==0
			licz=licz+1;
		endif
	end	
	planetemp(:,1:licz)=[];											%End of removal of unnecessary empty columns in temporary plane.
	
	planeout=[planeout; planetemp];									%Conjugation of output plane and temporary plane - creation of output plane 								

else																%Point_y coordinate is lower than midpoint of the 'plane'. Rotation data only to point_y if reflected.
	for i=1:point_x													%For loop in linear movement domain.
		for j=1:2.*point_y											%For loop in rotation domain.	
	
			d_x=point_x-i+1;										%Calculation of indexes of points symmetrical to currently analyzed point (i,j) with respect to the declared midpoint.
			d_y=point_y-j+1;		
			x_odb=point_x+d_x;
			y_odb=point_y+d_y;
		
			planeout(i,j)= plane(i,j);								%Coping of values from input plane to output plane.
			planetemp(x_odb,y_odb)=plane(i,j);						%Main reflection function. Coping the data from old coordinates to symmetrical coordinates in temporary plane.
		end
	end
	planetemp(1:point_x,:)=[];										%Removal of unnecessary empty lines in temporary plane.
	planeout=[planeout; planetemp];									%Conjugation of output plane and temporary plane - creation of output plane


endif

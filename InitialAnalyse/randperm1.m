function p = randperm1(n,m)

x=1:n;

for i=1:n*25
    
    z1=randi(n);
    z2=randi(n);
    
    y=x(z1);
    
    x(z1)=x(z2);
    x(z2)=y;
    
end;

p=sort(x(1:m));
    
end

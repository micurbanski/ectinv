function [x0,y0,quality0,vect_x,vect_y,quality1,results_a,results_p]=Initial_analyss (filename)


%***This function provides initial analyse for optimization of interpolation of data for eddy current tomography forward transformation. 
%This function proceeds in three main steps:
%
%	1. Verification, whether optimization for received datafile has already been conducted. When there are proper files, results are loaded and results are returned. Points 2 and 3 are not conducted anymore.
%	2. Detection of midpoint.  
%	3. Optimization of mesh for results interpolation. 
%
%***Input arguments:	'filename' 	-datafile from tomography setup
%
%***Output arguments:	'x0'		-x coordinate of midpoint
%						'y0'		-y coordinate of midpoint
%						'quality0'	-mean square error of data reflection with respect to detected midpoint (x0,y0)
%						'vect_x'	-vector of x coordinates of points in optimized mesh
%						'vect_y'	-vector of y coordinates of points in optimized mesh
%						'quality1'	-mean square error of total
%						'results_a'	-values of amplitude in points of optimized mesh
%						'results_p'	-values of phase in points of optimized mesh

tic
more off;
fprintf('\n \n Beginning of initial analyse')
[m filename_temp extension n]=fileparts (filename);				%Removing .txt from the input file name.

name1=[filename_temp "_qTmin.txt" 	];							%Creation of file names for data save.
name2=[filename_temp "_xnaj.txt" 	];	
name3=[filename_temp "_ynaj.txt" 	];
name4=[filename_temp "_Midpoint.txt" ];

clear m filename_temp extension n;								%Clearance of unused variables.


dane=csvread(filename);											%Load of data from input file.
[a,b]=size(dane);

t=max(dane(:, 2));
u=max(dane(:, 3));

wyn_a=zeros(t+1,u+1);
wyn_f=zeros(t+1,u+1);

for i=1:a														%Proper distribution of measurement results in function of linear movement and rotation.
	wyn_a(dane(i,2)+1,dane(i,3)+1)=dane(i,6);
	wyn_f(dane(i,2)+1,dane(i,3)+1)=atan(dane(i,4)/dane(i,5));
end

a_min=min(min(wyn_a));											%Results linearisation in the range (0,1)
a_max=max(max(wyn_a));
f_min=min(min(wyn_f));
f_max=max(max(wyn_f));


wyn_a=(wyn_a-a_min)/(a_max-a_min);
wyn_f=(wyn_f-f_min)/(f_max-f_min);								%End of results linearisation
fprintf ('\n \n Measurement data loaded and linearised')

%***************************************************************%Begin of step 1 - verification of previous optimization occurrence. 
ex=exist (name1)+exist (name2)+exist (name3) + exist (name4); 	%Verification of existence of output files. Function "exist" returns value 2 when input file exists.


if ex==8														%There should be 4 output files so ex should equal 8. Then all files should be read and return proper values.
			
	vect_x=load (name2,'-ascii');
	vect_y=load (name3,'-ascii');
	temp=load (name4, '-ascii');
	x0=temp(1,1);
	y0=temp(2,1);
	
	fprintf ('\n Previous optimisation data loaded')





%***************************************************************%End of step 1 - no proper data files noticed. Procedure of optimization is carrying on.	
else	
									




%***************************************************************%Begin of step 2 -detection of midpoint	
	fprintf ('\n Beginning of midpoint detection')
	for i=40:78													%Calculation of reflection with respect to points near the mid tables. 
		for j=40:60												%Values of tested point ranges are chosen experimentally.
			
			wyn_a_reflected=Rotation(wyn_a,i,j);				%Rotation of amplitude and phase data with respect to point (i,j)
			wyn_f_reflected=Rotation(wyn_f,i,j);
			
			[r t]=size(wyn_a_reflected);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
			[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

			n=min(r, o);
			m=min(t, p);										%Calculation of minimum sizes in both dimensions.
			wyn_a_reflected=resize(wyn_a_reflected,[n m]);		%Resize operation on all 4 arrays.
			wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
			wyn_f_reflected=resize(wyn_f_reflected,[n m]);			
			wyn_f_temp=resize(wyn_f,[n m]);						%***End of resize operation.
			
			t=Quality (wyn_a_temp,wyn_a_reflected,wyn_f_temp,wyn_f_reflected);	%Calculation of Mean Square Error between reflected data and initial data.
			
		
			fprintf ('\n Currently calculated point: %i %i . Quality=%f' , i,j,t)
			qual(i,j)=t;

			end						
	end															%End of calcKoniec of reflection with respect to the points near the mid of array.
	fprintf ('\n End of midpoint detection')
	qual (1:40,:)=[];											%Removal of unused (zero-valued) rows and columns in 'qual' array.
	qual (:,1:40)=[];	
	
	[num idx]=	min(qual(:));									%Detection of best quality of reflection.
	[x y]	 =	ind2sub(size(qual),idx);						%Detection of reflection point with best quality coordinates.
	
	x0=x+40;													%Real coordinates of midpoint (40 rows and columns were removed).
	y0=y+40;
	
	quality0=min(min(qual));
	save ("-text", name4, "x0", "y0");
	fprintf ('\n Midpoint coordinates are: %i and %i', x0, y0)
	%***********************************************************End of step 2. Midpoint has coordinates 'x0' and 'y0'.




	%***********************************************************Beginning of step 3 -optimization of mesh for results interpolation. 
	fprintf ('\n Beginning of mesh optimisation')
	%select=[8 3; 7 4; 11 6;  9 7; 14 7; 13 8; 15 8]; 			%Sizes of optimised meshes.
	select=[7 4];												%Tutaj wybralem jeden rozmiar siatki, tak aby funkcja zwraca�a co potrzeba.

	wyn_a=wyn_a (1:x0,:);										%Initial data cut to midpoint.
	wyn_f=wyn_f (1:x0,:);	

	

	[xgrid, ygrid] = meshgrid (1:size(wyn_a,2), 1:x0);
	
	for j1=1:(size(select)(1))									
		
		nrep=20;
		n=select(j1,1);
		m=select(j1,2);
		qTmin_=1e25;

															%Beginning of optimisation on base of previous results. 
		name_qT= ["qTmin_wzorzec.txt"];
		name_xnaj=["xnaj_wzorzec.txt"];
		name_ynaj=["ynaj_wzorzec.txt"];							
		
		qTminlog=load (name_qT, "-ascii");						%Reading data from reference data files.
		%qTminlog=getfield (qTmin,'qTmin');
		
		xnaj=load (name_xnaj);
		xnaj=getfield (xnaj, "xbest");		

		ynaj=load (name_ynaj);
		ynaj=getfield (ynaj, "ybest");							%End of data load.
		
		xsel =xnaj {n, m};										%Selecting proper values from reference data.
		ysel =ynaj {n,m};
		qTminlog=qTminlog(n,m)
		
	
		[xxopt,yyopt,quality]=MeshOpt(xgrid,ygrid,wyn_a,wyn_f,xsel,ysel);	
		quality=log10 (quality)
		if quality<=1.2.*qTminlog 								%ROMKU, ten warunek mo�e by� do zmiany oczywi�cie. 
			nrep=0;
			xnaj=xxopt;
			ynaj=yyopt;
		endif
															%End of optimisation on base of previous results.
		
																
		for i=1:nrep											%Beginning of optimization (Romku to jest to Twoje)

			xsel=GetMeshRand(n,size(wyn_a,2));
			ysel=GetMeshRand(m,x);
			fprintf('\n\n krok j1=%i krok j2=%i cykl=%i ',n,m,i)
			[xxopt,yyopt,quality]=MeshOpt(xgrid,ygrid,wyn_a,wyn_f,xsel,ysel);
			
		
			if quality<qTmin_
				qTmin_=quality;
				xnaj=xxopt;
				ynaj=yyopt;
			end
	  
			
		end

		qTmin(n,m)=qTmin_;
		

		xbest {n,m}=xnaj;
		ybest {n,m}=ynaj;

		save ("-text", name1, "qTmin");
		save ("-text", name2, "xbest");
		save ("-text", name3, "ybest");

	end % of for j1
	vect_x=xnaj;
	vect_y=ynaj;
	quality1=qTmin_;
	


endif

	
[xgrid, ygrid] = meshgrid (1:size(wyn_a,2), 1:x0);
xx_sel=xgrid(:,vect_x);
xx_sel=xx_sel(vect_y,:)

yy_sel=ygrid(:,vect_x);
yy_sel=yy_sel(vect_y,:);

wyn_a_sel=wyn_a(:,vect_x);
wyn_a_sel=wyn_a_sel(vect_y,:)

wyn_f_sel=wyn_f(:,vect_x);
wyn_f_sel=wyn_f_sel(vect_y,:)


temp_a=interp2(xx_sel,yy_sel,wyn_a_sel,xgrid,ygrid,'spline');
temp_p=interp2(xx_sel,yy_sel,wyn_f_sel,xgrid,ygrid,'spline');

results_a=Rotation(temp_a,x0,y0);
results_p=Rotation(temp_p,x0,y0);

[r t]=size(results_a);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

n=min(r, o);
m=min(t, p);										%Calculation of minimum sizes in both dimensions.
results_a=resize(results_a,[n m]);		%Resize operation on all 4 arrays.
wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
results_p=resize(results_p,[n m]);			
wyn_f_temp=resize(wyn_f,[n m]);		

quality1=sum(sum((wyn_a-results_a).^2))+sum(sum((wyn_f-results_p).^2));


wyn_a_reflected=Rotation(wyn_a,x0,y0);				%Rotation of amplitude and phase data with respect to point (i,j)
wyn_f_reflected=Rotation(wyn_f,x0,y0);



[r t]=size(wyn_a_reflected);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

n=min(r, o);
m=min(t, p);										%Calculation of minimum sizes in both dimensions.
wyn_a_reflected=resize(wyn_a_reflected,[n m]);		%Resize operation on all 4 arrays.
wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
wyn_f_reflected=resize(wyn_f_reflected,[n m]);			
wyn_f_temp=resize(wyn_f,[n m]);						%***End of resize operation.

quality0=Quality (wyn_a_temp,wyn_a_reflected,wyn_f_temp,wyn_f_reflected);	%Calculation of Mean Square Error between reflected data and initial data.

toc


function [x0,y0,quality0,vect_x,vect_y,quality1,results_a,results_p,wyn_a,wyn_f]=Input_data_analyse(filename)


%***This function provides initial analyse for optimization of interpolation of data for eddy current tomography forward transformation. 
%This function proceeds in three main steps:
%
%	1. Verification, whether optimization for received datafile has already been conducted. When there are proper files, results are loaded and results are returned. Points 2 and 3 are not conducted anymore.
%	2. Detection of midpoint.  
%	3. Optimization of mesh for results interpolation. 
%
%***Input arguments:	'filename' 	-datafile from tomography setup
%
%***Output arguments:	'x0'		-x coordinate of midpoint
%						'y0'		-y coordinate of midpoint
%						'quality0'	-mean square error of data reflection with respect to detected midpoint (x0,y0)
%						'vect_x'	-vector of x coordinates of points in optimized mesh
%						'vect_y'	-vector of y coordinates of points in optimized mesh
%						'quality1'	-mean square error of total
%						'results_a'	-values of amplitude in points of optimized mesh
%						'results_p'	-values of phase in points of optimized mesh
%						'wyn_a'		-normalised amplitude measurement results from input datafile
%						'wyn_f'		-normalised phase shift measurement results from input datafile

tic
more off;
fprintf('\n \n Initial input data analyse:')
[m filename_temp extension n]=fileparts (filename);				%Removing .txt from the input file name.

%name1=[filename_temp "_qTmin.txt" 	];							%Creation of file names for data save.
name2=[filename_temp "_xnaj.txt" 	];	
name3=[filename_temp "_ynaj.txt" 	];
name4=[filename_temp "_Midpoint.txt" ];
%name1_m=[filename_temp "_qTmin.mat" 	];						%Creation of file names for data save.
name2_m=[filename_temp "_xnaj.mat" 	];	
name3_m=[filename_temp "_ynaj.mat" 	];
name4_m=[filename_temp "_Midpoint.mat" ];

%select=[8 3; 7 4; 11 6;  9 7; 14 7; 13 8; 15 8]; 				%Sizes of optimised meshes.
select=[9 7];													%Tutaj wybralem jeden rozmiar siatki, tak aby funkcja zwraca�a co potrzeba.
n=select(1);
m=select(2);

clear  filename_temp extension ;								%Clearance of unused variables.

fprintf ('\n Load measurement data...');

dane=csvread(filename);											%Load of data from input file.

fprintf (' ok.\n');
fprintf (' Measurement data normalisation...');

[a,b]=size(dane);

t=max(dane(:, 2));
u=max(dane(:, 3));

wyn_a=zeros(t+1,u+1);
wyn_f=zeros(t+1,u+1);

for i=1:a														%Proper distribution of measurement results in function of linear movement and rotation.
	wyn_a(dane(i,2)+1,dane(i,3)+1)=dane(i,6);
	wyn_f(dane(i,2)+1,dane(i,3)+1)=atan(dane(i,4)/dane(i,5));
end

a_min=min(min(wyn_a));											%Results linearisation in the range (0,1)
a_max=max(max(wyn_a));
f_min=min(min(wyn_f));
f_max=max(max(wyn_f));


wyn_a=(wyn_a-a_min)/(a_max-a_min);
wyn_f=(wyn_f-f_min)/(f_max-f_min);								%End of results linearisation

fprintf (' ok.\n');
fprintf (' Measurement data analyse...');


%***************************************************************%Begin of step 1 - verification of previous optimization occurrence. 
ex=exist (name2)+exist (name3) + exist (name4); 	%Verification of existence of output files. Function "exist" returns value 2 when input file exists.


if ex==6												%There should be 3 output files so ex should equal 8. Then all files should be read and return proper values.
			
	vect_x=load (name2_m);	
	vect_x=getfield(vect_x,"vect_x");
	%vect_x=vect_x{n,m};
	
	vect_y=load (name3_m);	
	vect_y=getfield(vect_y,"vect_y");
	%vect_y=vect_y{n,m};

	temp=load (name4_m);
	x0=getfield(temp,"x0");
	y0=getfield(temp,"y0");
	
	fprintf ('ok. \n Previous data analyse loaded. \n')


%***************************************************************%End of step 1 - no proper data files noticed. Procedure of optimization is carrying on.	
else	
									




%***************************************************************%Begin of step 2 -detection of midpoint	
	fprintf ('\n Midpoint detection... \n');
	
	if (exist(name4_m)==2)											%Loading only midpoin coordinates
		temp=load (name4_m);
		x0=getfield(temp,"x0");
		y0=getfield(temp,"y0");
	else
		for i=40:78													%Calculation of reflection with respect to points near the mid tables. 
			for j=40:60												%Values of tested point ranges are chosen experimentally.
			
				wyn_a_reflected=Rotation(wyn_a,i,j);				%Rotation of amplitude and phase data with respect to point (i,j)
				wyn_f_reflected=Rotation(wyn_f,i,j);
				
				[r t]=size(wyn_a_reflected);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
				[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

				n=min(r, o);
				m=min(t, p);										%Calculation of minimum sizes in both dimensions.
				wyn_a_reflected=resize(wyn_a_reflected,[n m]);		%Resize operation on all 4 arrays.
				wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
				wyn_f_reflected=resize(wyn_f_reflected,[n m]);			
				wyn_f_temp=resize(wyn_f,[n m]);						%***End of resize operation.
				
				t=Quality (wyn_a_temp,wyn_a_reflected,wyn_f_temp,wyn_f_reflected);	%Calculation of Mean Square Error between reflected data and initial data.
				
			
				% fprintf ('\n Currently calculated point: %i %i . Quality=%f' , i,j,t)
				fprintf ('*');
				
				qual(i,j)=t;

			end		
			fprintf ('\n');
		end															%End of calcKoniec of reflection with respect to the points near the mid of array.
	
		fprintf ('\n Midpoint detection done. \n')
		qual (1:40,:)=[];											%Removal of unused (zero-valued) rows and columns in 'qual' array.
		qual (:,1:40)=[];	
		
		[num idx]=	min(qual(:));									%Detection of best quality of reflection.
		[x y]	 =	ind2sub(size(qual),idx);						%Detection of reflection point with best quality coordinates.
	
		x0=x+40;													%Real coordinates of midpoint (40 rows and columns were removed).
		y0=y+40;
	
		save ("-text", name4, "x0", "y0");
		save ("-V7", name4_m,"x0", "y0" );
		
	endif
	fprintf (' Midpoint coordinates are: %i and %i \n', x0, y0);
    %fprintf ('quality of midpoint: %e \n', quality0);
	
    %***********************************************************End of step 2. Midpoint has coordinates 'x0' and 'y0'.

	%***********************************************************Beginning of step 3 -optimization of mesh for results interpolation. 
	fprintf ('\n Important mesh point detection: optimisation...')
							

%	q=size(wyn_a)(1);
 %   	q=q-x0;
  %  	q=round ((y0-q)./2);
	%subplot (1,3,1)
	%pcolor (wyn_a);
	
   % 	wyn_a(1:q,:)=[];
	%subplot (1,3,2)
	%pcolor (wyn_a);
    %	wyn_f(1:q,:)=[];
	

	%x1=x0-q;
	wyn_a=wyn_a (1:x0,:);
	%subplot (1,3,3)
	%pcolor (wyn_a);										%Initial data cut to midpoint.
	wyn_f=wyn_f (1:x0,:);	

	

	[xgrid, ygrid] = meshgrid (1:size(wyn_a,2), 1:x0);
	nrep=1	;
	for j1=1:(size(select)(1))									
		
		
		n=select(j1,1);
		m=select(j1,2);
		qTmin_=1e25;

		%Beginning of optimisation on base of previous results calculated for datafile 'Walek_Fe8_2013-02-27_22-50-36.txt'
		%with discrete linear optimisation with 50 repeats with midpoint [46 51];
		name_qT  =["qTmin_wzorzec.mat"];
		name_xnaj=["xnaj_wzorzec.mat"];
		name_ynaj=["ynaj_wzorzec.mat"];							
		
		ex=exist(name_qT)+exist(name_xnaj)+exist(name_ynaj);
		if (ex==6)
			fprintf ('\n Beginning of optimisation on base of previous results \n')	
			qTminlog=load (name_qT);						%Reading data from reference data files.
			qTminlog=getfield (qTminlog,'qTmin_wzorzec');
			
			xnaj=load (name_xnaj);
			xnaj=getfield (xnaj, "xbest");		

			ynaj=load (name_ynaj);
			ynaj=getfield (ynaj, "ybest");							%End of data load.
			
			xsel =xnaj {n, m};										%Selecting proper values of points indexest and quality value from reference data.
			ysel =ynaj {n,m};
			qTminlog=qTminlog(n,m);
			qTminlog=10^qTminlog;									%In reference file quality is saved as a log10 of MSE value.
			
			fprintf (' Reference quality value is %f \n', qTminlog);
		
			[xxopt,yyopt,quality]=MeshOpt(xgrid,ygrid,wyn_a,wyn_f,xsel,ysel);	%Running optimisation on reference data as initial state (nor random generation of xsel and ysel).
			
			fprintf ( 'Quality of optimsed mesh is %f \n',quality);
			
			
			if quality<= 1.2.*qTminlog 					%ROMKU, ten warunek mo�e by� do zmiany oczywi�cie. 
				fprintf (' Optimisation on previous results succesfull \n');
				nrep=0;
				xnaj=xxopt;
				ynaj=yyopt;
			else
				fprintf (' Optimisation on previous results unsuccesfull. \n');
			endif											%End of optimisation on base of previous results.
		else
			fprintf ('\n No reference file found.');
		endif
															
		
																
		for i=1:nrep											%Beginning of optimization (Romku to jest to Twoje)
			
			xsel=GetMeshRand(n,size(wyn_a,2));
			ysel=GetMeshRand(m,x0);
			fprintf('\n\n krok j1=%i krok j2=%i cykl=%i ',n,m,i)
			[xxopt,yyopt,quality]=MeshOpt(xgrid,ygrid,wyn_a,wyn_f,xsel,ysel);
			
			if quality<qTmin_
				qTmin_=quality;
				xnaj=xxopt;
				ynaj=yyopt;
			end			
		end

		%qTmin(n,m)=qTmin_;
		

		xbest {n,m}=xnaj;
		ybest {n,m}=ynaj;

		%save ("-text", name1, "qTmin");
		

	end % of for j1
	vect_x=xnaj;
	vect_y=ynaj;
	
	save ("-text", name2, "vect_x");
	save ("-text", name3, "vect_y");
	save ("-V7", name2_m, "vect_x");
	save ("-V7", name3_m, "vect_y");


endif

	
[xgrid, ygrid] = meshgrid (1:size(wyn_a,2), 1:x0);
xx_sel=xgrid(:,vect_x);
xx_sel=xx_sel(vect_y,:);

yy_sel=ygrid(:,vect_x);
yy_sel=yy_sel(vect_y,:);

wyn_a_sel=wyn_a(:,vect_x);
wyn_a_sel=wyn_a_sel(vect_y,:);

wyn_f_sel=wyn_f(:,vect_x);
wyn_f_sel=wyn_f_sel(vect_y,:);


temp_a=interp2(xx_sel,yy_sel,wyn_a_sel,xgrid,ygrid,'spline');
temp_p=interp2(xx_sel,yy_sel,wyn_f_sel,xgrid,ygrid,'spline');

results_a=Rotation(temp_a,x0,y0);
results_p=Rotation(temp_p,x0,y0);

[r t]=size(results_a);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

n=min(r, o);
m=min(t, p);										%Calculation of minimum sizes in both dimensions.
results_a=resize(results_a,[n m]);					%Resize operation on all 4 arrays.
wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
results_p=resize(results_p,[n m]);			
wyn_f_temp=resize(wyn_f,[n m]);		

quality1=sum(sum((wyn_a_temp-results_a).^2))+sum(sum((wyn_f_temp-results_p).^2));

%Calculation of quality0 - comparision of input data symmetry with respect to midpoint
wyn_a_reflected=Rotation(wyn_a,x0,y0);				%Rotation of amplitude and phase data with respect to point (i,j)
wyn_f_reflected=Rotation(wyn_f,x0,y0);



[r t]=size(wyn_a_reflected);						%***Beginning of resize of output arrays. Resize is conducted in order to avoid errors related to nonconformed arrays dimensions during their comparision.
[o p]=size(wyn_a);									%Calculation of sizes of reflected array and initial array.

n=min(r, o);
m=min(t, p);										%Calculation of minimum sizes in both dimensions.
wyn_a_reflected=resize(wyn_a_reflected,[n m]);		%Resize operation on all 4 arrays.
wyn_a_temp=resize(wyn_a,[n m]);						%wyn_a_temp and wyn_f_temp variables are used in order to avoid overwrite variables wyn_a and wyn_f.
wyn_f_reflected=resize(wyn_f_reflected,[n m]);			
wyn_f_temp=resize(wyn_f,[n m]);						%***End of resize operation.

quality0=Quality (wyn_a_temp,wyn_a_reflected,wyn_f_temp,wyn_f_reflected);	%Calculation of Mean Square Error between reflected data and initial data.

fprintf (' Midpoint coordinates are %i and %i \n ', x0, y0);
fprintf ('Optimised mesh coordinates are \n \t')
fprintf (' %i', vect_x)
fprintf (' in rotation domain \n \t');
fprintf (' %i', vect_y)
fprintf (' in linear movement domain \n \t');
fprintf ('Quality of midpoint rotation is %f \n \t', quality0);
fprintf ('Quality of rotation and data interpolation is %f \n \t', quality1);

% l=size(results_a)(1)
% q=l-x0
% q=(y0-q)./2
% results_a(1:q,:)=[];


results_a=wyn_a_sel;
results_p=wyn_f_sel;
fprintf ('\n\n Initial analyse done. \n \n ');
toc
fprintf ('---------------------\n\n');

subroutine integ(model,solver,dt,transient)
 use defutils
 type(model_t) :: model
 type(solver_t) :: solver
 real(kind=dp) :: dt
 logical :: transient

 real(kind=dp) :: sumzr,sumzi, vol,sumbr,sumbi
 integer :: i,n,nd
 logical :: found
 type(element_t), pointer :: element

 logical :: exist

 sumzr = 0._dp
 sumzi = 0._dp

 sumbr = 0._dp
 sumbi = 0._dp

 vol = 0
 
 do i=1,getnofactive()
   element => getactiveelement(i)
   IF(.NOT.GetLogical(GetBodyParams(),'Flux Integration Body',Found)) CYCLE
   n  = getelementnofnodes()
   nd = getelementnofdofs()
   call psumz(element,n,nd)
 end do 

 sumzr = parallelreduction(sumzr)
 sumzi = parallelreduction(sumzi)

 sumbr = parallelreduction(sumbr)
 sumbi = parallelreduction(sumbi)

 vol   = parallelreduction(vol)

 if (parenv % mype==0 ) then
   print*,'ax: ', sumzr,sumzi, sumbr, sumbi, vol

   inquire(file="fort.dat", exist=exist)
   if (exist) then
	open(1, file="magn_flux.dat", status='old',access='append')
   else
	open(1, file="magn_flux.dat", status="new", action="write")
   end if
   write(1,*) sumzr, sumzi, sumbr, sumbi, vol
   close(1)
 endif

CONTAINS

  SUBROUTINE psumz(Element,n,nd)
    TYPE(element_t), pointer :: Element
    INTEGER :: n,nd

    real(kind=dp) :: Basis(nd), dBasisdx(nd,3), RotW(nd,3), BaseW(nd,3)
    real(kind=dp) :: u,v,w,detj,Bre_z(nd), Bim_z(nd), Pre(nd), Pim(nd)
    type(nodes_t), save :: nodes
    integer :: t
    logical :: stat
    TYPE(GaussIntegrationPoints_t) :: IP

    CALL GetElementNodes(Nodes)

    CALL GetLocalSolution(Pre, 'p re')
    CALL GetLocalSolution(Pim, 'p im')

    CALL GetLocalSolution(Bre_z, 'Magnetic Flux Density Re e 3')
    CALL GetLocalSolution(Bim_z, 'Magnetic Flux Density Im e 3')

    !Numerical integration:
    !----------------------
    IP = GaussPoints(Element,8)
    DO t=1,IP % n
      stat = ElementInfo( Element, Nodes, IP % U(t), IP % V(t), &
                IP % W(t), detJ, Basis, dBasisdx )

      vol = vol + ip % s(t) * detj
      sumbr = sumbr + IP % s(t)*Detj*SUM(Bre_z(1:n) * Basis(1:n))
      sumbi = sumbi + IP % s(t)*Detj*SUM(Bim_z(1:n) * Basis(1:n))
 
      CALL GetEdgeBasis(Element,BaseW,RotW,Basis,dBasisdx)

      sumzr = sumzr + IP % s(t)*Detj*SUM(Pre(n+1:nd) * RotW(1:nd-n,3))
      sumzi = sumzi + IP % s(t)*Detj*SUM(Pim(n+1:nd) * RotW(1:nd-n,3))
    END DO
  END SUBROUTINE psumz
end subroutine integ

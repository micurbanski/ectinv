FUNCTION currdens1(model, n, args) RESULT(curr)

    Use DefUtils
    IMPLICIT None
    Type(model_t) ::model
    INTEGER :: n
    REAL(KIND=dp) :: x, y, z, t, args(4), curr
    
    x = args(1)
    y = args(2)
    z = args(3)
    t = args(4)
    
    curr = -y/sqrt(x*x + y*y)
    
END FUNCTION currdens1

FUNCTION currdens2(model, n, args) RESULT(curr)

    Use DefUtils
    IMPLICIT None
    Type(model_t) ::model
    INTEGER :: n
    REAL(KIND=dp) :: x, y, z, t, args(4), curr
    
    x = args(1)
    y = args(2)
    z = args(3)
    t = args(4)
    
    curr = x/sqrt(x*x + y*y)
    
END FUNCTION currdens2
function [ wynik ] = BATCHFEMWrite(catalog, n)
%BATCHWRITE prepare batch files in catalog for core n

a{1}='#!/bin/sh -f';
a{2}=['taskset -c ',num2str(n,'%i'),' cat perme_cond_p1.f90 perme_cond_p2.f90 perme_cond_p3.f90 > perme_cond.f90'];
a{3}=['taskset -c ',num2str(n,'%i'),' elmerf90 perme_cond.f90 -o perme'];
a{4}=['taskset -c ',num2str(n,'%i'),' ElmerSolver'];
a{5}=['taskset -c ',num2str(n,'%i'),' touch FEM_done.txt'];

nazwa=[catalog '/FEMphase_go'];

FID = fopen(nazwa, 'w');
fprintf(FID, '%s\n', a{:});
fclose(FID);

[output, text]=system(["chmod 755 " nazwa]);    

wynik=1;

end

